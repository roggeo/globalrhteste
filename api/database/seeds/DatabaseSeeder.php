<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vendedor')->insert([
        	'id' => 1,
            'nome' => 'João Marcelo',
        ]);

        DB::table('vendedor')->insert([
        	'id' => 2,
            'nome' => 'Luciana Costa',
            'indicador' => 1
        ]);
    }
}
