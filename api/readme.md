# GlobalRH teste (BINÁRIA - Plano de carreira)

## Requisito de ambiente

Cheque seu ambiente:

    `php --version`

    `node --version`


API (Backend):

- PHP 7.1.3+

- MySql Server

- Habilitar a extensão OpenSSL php.ini

- Habilitar a extensão PDO  php.ini

- Habilitar a extensão Mbstring


APP (Frontend):

- nodejs 8.15.0+


**Lembre de setar php na variável de ambiente do sistema**

- PHP 7.1.3+

- Habilitar a extensão OpenSSL php.ini

- Habilitar a extensão PDO  php.ini

- Habilitar a extensão Mbstring


## Instalando e Rodando a aplicação no localhost (Back e Front)

**API (Backend):**

Por favor configure os dados de acesso do banco de dados MySql no arquivo ``.env``

e crie um banco de dados com nome ``db_globalrh_teste``:

   `CREATE SCHEMA db_globalrh_teste DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;`


Agora use os comandos no seu terminal:

   `cd api`

   `php composer.phar install`

   `php artisan migrate`

   `php artisan db:seed`

   `php -S localhost:8000 -t public`


Pronto, está rodando na url ``localhost:8000``.


**APP (Frontend):**

Use os comandos no seu terminal:

   `cd web`

   `php -S localhost:3000 -t build`

    
Acesse: [localhost:3000](http://localhost:3000)


## Documentação API

Para testar as funcionalidados da API você pode usar PostMan.

**Listar todos vendedores**

GET

    http://localhost:8000/api/v1/vendedores



**Listar um vendedor**

GET

    http://localhost:8000/api/v1/vendedores/[Id_Vendedor]


**Inserir um vendedor**

POST FormData: ``nome``, ``indicador``.

    http://localhost:8000/api/v1/vendedores


**Atualizar um vendedor**

PUT FormData: ``nome``, ``indicador``.

    http://localhost:8000/api/v1/vendedores/[Id_Vendedor]


**Deletar vendedor**

DELETE.

    http://localhost:8000/api/v1/vendedores/[Id_Vendedor]


**Relatórios**

Relatorio rede de um vendedor:

GET

    http://localhost:8000/api/v1/vendedores/relatorio/vendedor/[Id_Vendedor]


Relatorio rede geral:

GET

    http://localhost:8000/api/v1/vendedores/relatorio/rede

