<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return 'API - Global RH Solutions Teste (v1)';
});

 $router->group(['prefix' => 'api/v1'], function () use ($router) {
    $router->group(['prefix' => 'vendedores'], function () use ($router) {

    	//GET
        $router->get('/', ['uses' => 'Api\V1\VendedorController@index']);
        $router->get('/{id:[0-9]+}', ['uses' => 'Api\V1\VendedorController@show']);
        $router->get('/permitidos', ['uses' => 'Api\V1\VendedorController@permitidosParaIndicar']);
        $router->get('/relatorio/vendedor/{id:[0-9]+}', ['uses' => 'Api\V1\RelatorioVendedorController@unicoVendedor']);
        $router->get('/relatorio/rede', ['uses' => 'Api\V1\RelatorioVendedorController@redeBinaria']);

        //POST
        $router->post('/', ['uses' => 'Api\V1\VendedorController@create']);

        //PUT
        $router->put('/{id:[0-9]+}', ['uses' => 'Api\V1\VendedorController@update']);

        //DELETE
        $router->delete('/{id:[0-9]+}', ['uses' => 'Api\V1\VendedorController@destroy']);
    });
});