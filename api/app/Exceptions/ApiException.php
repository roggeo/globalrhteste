<?php

namespace App\Exceptions;

use App\Exceptions\MessageException;
use Illuminate\Http\Exceptions\HttpResponseException;

class ApiException
{
	public static function render($msg)
	{
		$message = new MessageException($msg);
		throw new HttpResponseException(response()->json($message->toArray(), $message->getCode()));
	}
}