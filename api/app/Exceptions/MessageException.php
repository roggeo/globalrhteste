<?php

namespace App\Exceptions;

class MessageException
{
	const MSG_VENDEDOR_NAO_EXISTE = 1;
	const MSG_REGISTRO_NAO_EXCLUIDO = 2;
	const MSG_PAGINA_NAO_ENCONTRADA = 3;
	const MSG_REQUISICAO_INVALIDA = 4;

	private $code;

	private $message;

	private static $mensagens = [
		1 => ['code' => 400, 'msg' => 'Vendedor não existe.'],
		2 => ['code' => 400, 'msg' => 'Registro não foi excluído!'],
		3 => ['code' => 404, 'msg' => 'Página não encontrada.'],
		4 => ['code' => 400, 'msg' => 'Requisição inválida.']
	];

	public function __construct($codeMSG)
	{
		$message = self::$mensagens[$codeMSG];
		$this->code = $message['code'];
		$this->message = $message['msg'];
	}

	public function getCode()
	{
		return $this->code;
	}

	public function getMessage()
	{
		return $this->message;
	}

	public function toArray()
	{
		return [
			'code' => $this->getCode(),
	    	'message' => $this->getMessage(),
		];

	}
}