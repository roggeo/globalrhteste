<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vendedor extends Model
{

	protected $table = 'vendedor';

    /**
     * Atributos assinável.
     *
     * @var array
     */
    protected $fillable = [
        'nome', 'indicador', 'status',
    ];

    /**
     * Obter o nome da tabela.
     */
    public static function getTableName()
	{
		return (new self())->getTable();
	}
}
