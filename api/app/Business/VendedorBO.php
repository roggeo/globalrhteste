<?php

namespace App\Business;

use App\Models\Vendedor;
use App\To\VendedorTO;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\VendedorJson;
use App\Http\Resources\VendedorCollection;
use App\Exceptions\ApiException;
use App\Exceptions\MessageException;

class VendedorBO
{
	/**
	 * Método construtor da classe.
	 */
	public function __construct()
	{

	}

	/**
	 * Método reponsável por construir uma instância da classe.
	 * @return App\Business\VendedorBO
	 */
	public static function newInstance()
	{
		return new VendedorBO();
	}

	/**
	 * Obter todos vendedores ativos.
	 * 
	 * @return array
	 */
	public function getVendedoresAtivos()
	{
		$vendedores = Vendedor::where('status', '1')->get();
		return new VendedorCollection($vendedores);
	}

    /**
     * Obter todos vendedores permitidos para fazer indicação.
     * 
     * @return array
     */
    public function getVendedoresPermitidosParaIndicar()
    {
        $vendedores = Vendedor::where('status', '1')->get();
        $permitidos = [];

        foreach ($vendedores as $vendedor) {
            if (Vendedor::where('indicador', $vendedor->id)->get()->count() < 2) {
                $permitidos[] = $vendedor;
            }
        }

        return ['data' => $permitidos];
    }

	/**
     * Visualizar dados de um vendedor.
     *
     * @param int $id
     * @return array
     */
	public function getVendedorPorId($id)
	{
    	$vendedor = Vendedor::find($id);
		return new VendedorJson($vendedor);
    }

	/**
     * Criar um vendedor.
     * 
     * @param VendedorTO $vendedorTO
     * @return array
     */
    public function salvarVendedor($vendedorTO)
    {
    	if (!array_key_exists('nome', $vendedorTO->toArray())
    		or !array_key_exists('indicador', $vendedorTO->toArray())) {
    			ApiException::render(MessageException::MSG_REQUISICAO_INVALIDA);
    	}

   		$vendedor = Vendedor::create($vendedorTO->toArray());
		return new VendedorJson($vendedor);
    }

	/**
     * Atualizar dados de um vendedor.
     * 
     * @param int $id
     * @param VendedorTO $vendedorTO
     * @return array
     */
    public function atualizarVendedor($id, $vendedorTO)
    {
    	if (!array_key_exists('nome', $vendedorTO->toArray())
    		or !array_key_exists('indicador', $vendedorTO->toArray())) {
    			ApiException::render(MessageException::MSG_REQUISICAO_INVALIDA);
    	}

   		Vendedor::find($id)->update($vendedorTO->toArray());

   		$vendedor = Vendedor::find($id);

		return new VendedorJson($vendedor);
    }

	/**
     * Deletar um vendedor.
     * 
     * @param int $id
     * @return array
     */
    public function deletarVendedor($id)
    {
    	$vendedor = Vendedor::find($id);
		
		if ($vendedor && $vendedor->delete()) {
			return ['code' => 200, 'message' => 'Registro excluído!'];
		}

		ApiException::render(MessageException::MSG_REGISTRO_NAO_EXCLUIDO);
    }
}