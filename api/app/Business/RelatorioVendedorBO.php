<?php

namespace App\Business;

use App\Models\Vendedor;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\VendedorJson;
use App\Http\Resources\VendedorCollection;
use App\Exceptions\ApiException;
use App\Exceptions\MessageException;

class RelatorioVendedorBO
{
	/**
	 * Valor de pontos por indicação de novos vendedores.
	 */
	const PONTOS_INDICACAO = 500;

	/**
	 * Limite de pernas binárias por vendedor.
	 * @var int
	 */
	const LIMITE_PERNAS = 2;

	/**
	 * Níveis de plano de carreira.
	 * @var array
	 */
	private static $nivelPlanoCarreira = [
		['nivel' => 'Vendedor', 	'requisito' => [0, 0]],
		['nivel' => 'Bronze', 		'requisito' => [1, 500]],
		['nivel' => 'Prata', 		'requisito' => [501, 1000]],
		['nivel' => 'Ouro', 		'requisito' => [1001, 2000]],
		['nivel' => 'Diamante', 	'requisito' => [2001, 99999999]]
	];


	/**
	 * Método construtor da classe.
	 */
	public function __construct()
	{

	}

	/**
	 * Método reponsável por construir uma instância da classe.
	 * @return App\Business\RelatorioVendedorBO
	 */
	public static function newInstance()
	{
		return new RelatorioVendedorBO();
	}

	/**
	 * Obter relatorio de um unico vendedor.
	 * 
	 * @param int $id
	 * @return array
	 */
	public function getRelatorioVendorPorId($id)
	{
		$indicador = Vendedor::where('status', '1')
			->where('id', $id)->first();

		if (!$indicador) {
			ApiException::render(MessageException::MSG_VENDEDOR_NAO_EXISTE);
		}

		return $this->getRelatorioRedeBinariaPorIndicador($indicador);

	}

	/**
	 * Obter relatório da rede binária de vendedores.
	 * 
	 * @param object $indicador
	 * @return array
	 */
	public function getRelatorioRedeBinariaPorIndicador($indicador)
	{
		$dadosRedeBinaria = $this->getDadosRedeBinariaIndicador($indicador);
		return $this->formatarDadosRedeBinariaIndicador($dadosRedeBinaria);
	}

	/**
	 * Obter relatório de todos vendedores.
	 * 
	 * @return array
	 */
	public function getRelatorioTodaRedeBinaria()
	{
		$lista = [];
		$vendedores = Vendedor::where('status', '1')->get();
		
		foreach ($vendedores as $vendedor) {
			$lista[] = $this->getDetalheNivelVendedor($vendedor);
		}

		return $lista;

	}


	/**
     * Obter nivel de vendedor conforme pontuação.
     *
	 * @param object $vendedor
	 */
	public function getNivelVendedor($pontos)
	{
		$nomeNivel = 'Vendedor';
		$niveis = self::$nivelPlanoCarreira;

		foreach ($niveis as $nivel) {
			if ($pontos >= $nivel['requisito'][0] && $pontos <= $nivel['requisito'][1]) {
				$nomeNivel = $nivel['nivel'];
			}
		}

		return $nomeNivel;
	}


	/**
	 * @param object $vendedor
	 */
	private function getDetalheNivelVendedor($vendedor)
	{
		$dados = $this->getRelatorioRedeBinariaPorIndicador($vendedor);
		$pontos = 0;
		$totalEsquerda =  count($dados['data']['rede']['esquerda']);
		$totalDireita = count($dados['data']['rede']['direita']);

		if (($totalDireita < $totalEsquerda) && $totalDireita >= 1) {
			$pontos = self::PONTOS_INDICACAO * $totalDireita;

		} elseif ($totalEsquerda >= 1) {
			$pontos = self::PONTOS_INDICACAO * $totalEsquerda;
		}

		return [
			'data' => [
				'id_vendedor' => $vendedor->id,
				'vendedor' => $vendedor->nome,
				'pontos' => $pontos,
				'nivel' => $this->getNivelVendedor($pontos)
			]
		];
	}

	/**
	 * @param object $indicador
	 */
	private function getDadosRedeBinariaIndicador($indicador)
	{
		$pernaEsquerda = [];
		$pernaDireita = [];


		$indicados = Vendedor::where('status', '1')
			->where('indicador', $indicador->id)
			->orderBy('id', 'ASC')
			->get();

		
		$i = self::LIMITE_PERNAS;

		foreach ($indicados as $indicado) {
			if ($i == 0) break;

			if ($i == self::LIMITE_PERNAS) {
				$pernaEsquerda[] = [
					'nome' => $indicado->nome,
					'perna'	=> $this->getPernasBinaria($indicado->id)
				];
			} else {
				$pernaDireita[] = [
					'nome' => $indicado->nome,
					'perna'	=> $this->getPernasBinaria($indicado->id)
				];
			}

			$i--;
		}

		return [
			'id_indicador' => $indicador->id,
			'indicador' => $indicador->nome,
			'rede' => [
				'perna_esquerda' => $pernaEsquerda,
				'perna_direita' => $pernaDireita
			]
		];
	}


	/**
	 * @param int $id
	 */
	private function getPernasBinaria($id)
	{
		$retorno = [];

		$indicados = Vendedor::where('status', '1')
			->where('indicador', $id)
			->get();

		if (!$indicados) return null;

		$i = self::LIMITE_PERNAS;

		foreach ($indicados as $indicado) {
			if ($i == 0) break;
			$retorno[] = [
				'nome' => $indicado->nome,
				'perna'	=> $this->getPernasBinaria($indicado->id)
			];
			$i--;
		}

		return $retorno;

	}

	/**
	 * @param array $dados
	 * @return array
	 */
	private function formatarDadosRedeBinariaIndicador($dados)
	{		
		$esquerda = [];
		$direita = [];

		array_walk_recursive($dados['rede']['perna_esquerda'],
			function($v, $k) use (&$esquerda) {
			
				array_push($esquerda, $v);
			
		});

		array_walk_recursive($dados['rede']['perna_direita'],
			function($v, $k) use (&$direita) {
			
				array_push($direita, $v);
			
		});

		return [
			'data' => [
				'id_vendedor' => $dados['id_indicador'],
				'vendedor' => $dados['indicador'],
				'pontos_por_indicado' => self::PONTOS_INDICACAO,
				'rede' => [
					'esquerda' => $esquerda,
					'direita' => $direita
				]
			]
		];
	}

}