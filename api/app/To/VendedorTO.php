<?php

namespace App\To;

class VendedorTO
{
	private $nome;

	private $indicador;

	private $status = 1;

	/**
	 * Método construtor da classe.
	 */
	public function __construct($request)
	{
		$this->nome = $request->input('nome');
		$this->indicador = $request->input('indicador');
	}

	/**
	 * Método reponsável por construir uma instância da classe.
	 * @return App\Business\VendedorTO
	 */
	public static function newInstance($request)
	{
		return new VendedorTO($request);
	}

	/**
	 * Obter todos vendedores ativos.
	 * 
	 * @return array
	 */
	public function toArray()
	{
		return [
			'nome' => $this->nome,
			'indicador' => ((int) $this->indicador == 0) ? null : $this->indicador,
			'status' => (int) $this->status
		];
	}
}