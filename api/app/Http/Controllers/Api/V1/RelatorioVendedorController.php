<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Business\RelatorioVendedorBO;

class RelatorioVendedorController extends Controller
{
	/**
     * @var App\Business\RelatorioVendedorBO
     */
	private $relatorioVendedorBO;

	/**
     * Construtor do controlador.
     *
     * @return void
     */
    public function __construct()
    {
    	$this->relatorioVendedorBO = RelatorioVendedorBO::newInstance();
    }

	public function unicoVendedor($id)
	{
		$relatorio = $this->relatorioVendedorBO->getRelatorioVendorPorId($id);
		return response()->json($relatorio);
	}

	public function redeBinaria()
	{
		$relatorio = $this->relatorioVendedorBO->getRelatorioTodaRedeBinaria();
		return response()->json($relatorio);
	}
}