<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Business\VendedorBO;
use App\To\VendedorTO;
use Illuminate\Http\Request;

class VendedorController extends Controller
{

    /**
     * @var App\Business\VendedorBO
     */
	private $vendedorBO;

    /**
     * Construtor do controlador.
     *
     * @return void
     */
    public function __construct()
    {
    	$this->vendedorBO = VendedorBO::newInstance();
    }

    /**
     * Vizualizar todos vendedores.
     */
    public function index()
    {
    	$vendedores = $this->vendedorBO->getVendedoresAtivos();
    	return response()->json($vendedores);
    }

    /**
     * Vizualizar todos vendedores permitidos para fazer indicação.
     */
    public function permitidosParaIndicar()
    {
        $vendedores = $this->vendedorBO->getVendedoresPermitidosParaIndicar();
        return response()->json($vendedores);
    }

	/**
     * Vizualizar um vendedor.
     *
     * @param $id
     */
    public function show($id)
    {    	
    	$vendedor = $this->vendedorBO->getVendedorPorId($id);
    	return response()->json($vendedor);
    }

	/**
     * Criar um vendedor.
	 *
     * @param Request $request
     */
    public function create(Request $request)
    {
    	$vendedorTO = VendedorTO::newInstance($request);
    	$vendedor = $this->vendedorBO->salvarVendedor($vendedorTO);
    	return response()->json($vendedor);
    }

	/**
     * Atualizar um vendedor.
	 *
     * @param Request $request
     */
    public function update($id, Request $request)
    {
    	$vendedorTO = VendedorTO::newInstance($request);
    	$vendedor = $this->vendedorBO->atualizarVendedor($id, $vendedorTO);
    	return response()->json($vendedor);
    }

	/**
     * Deletar um vendedor.
     *
     * @param int $id
     */
    public function destroy($id)
    {
    	$vendedor = $this->vendedorBO->deletarVendedor($id);
    	return response()->json($vendedor);
    }
}
