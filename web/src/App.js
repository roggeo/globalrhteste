import React from 'react';
import { BrowserRouter as Router, Route} from "react-router-dom";
import './App.css';
import HomePage from './pages/HomePage';
import VendedorPage from './pages/VendedorPage';
import RelatorioVendedorPage from './pages/RelatorioVendedorPage';
import HeaderComponent from './components/HeaderComponent';
import FooterComponent from './components/FooterComponent';

function App() {
  return (
    <Router>
      <div>
        <HeaderComponent />
        <Route exact path="/" component={HomePage} />
        <Route path="/vendedor" component={VendedorPage} />
        <Route path='/relatorio/vendedor/:id' component={RelatorioVendedorPage} />
        <FooterComponent />
      </div>
    </Router>
  );
}

export default App;
