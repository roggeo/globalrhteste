import React, {Component} from 'react';
import { Table } from 'react-bootstrap';
import { Link } from "react-router-dom";
import ConfigAmbiente from './../ConfigAmbiente';
import './../App.css';

class RelatorioComponent extends Component {

	constructor(props) {
      super(props);

      this.state = {
      	error: null,
      	isLoaded: false,
      	dadosRede: []
      }
  	}

	getRelatorioRede(){		
		fetch(ConfigAmbiente.API_URL + '/vendedores/relatorio/rede')
		  .then(function(response) {
		    return response.json();
		  })
		  .then((json) => {
		    this.setState({
				dadosRede: json,
				isLoaded: true
			});
		  });
	}

	componentDidMount(){
		this.getRelatorioRede();		
	}

	render() {

		const { error, isLoaded, dadosRede } = this.state;

		if (error) {
	      return <div>Error: {error.message}</div>;
	    } else if (!isLoaded) {
	      return <div className="textCenter">Carregando...</div>;
	    } else {
			return (
				<div class="AppContent">
					<h2>Relatório rede binária de todos vendedores</h2>
					<Table striped bordered hover>
						<thead>
							<tr>
								<th>Vendedor</th>
								<th>Postos Perna Menor</th>
								<th>Nível</th>
							</tr>
						</thead>
						<tbody>
							{dadosRede.map(val =>
								<tr key={val.data.id_vendedor}>
									<td>
										<Link to={'/relatorio/vendedor/' + val.data.id_vendedor}>{val.data.vendedor}</Link>
									</td>
									<td>{val.data.pontos} pts</td>
									<td>{val.data.nivel}</td>
								</tr>
							)}
							<tr><td colSpan="3"><b>Total registros: {dadosRede.length}</b></td></tr>			
						</tbody>
					</Table>
				</div>
			);
		}
	}
}

export default RelatorioComponent;