import React, {Component} from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import './../App.css';

class FooterComponent extends Component {
	render() {
		return (
			<Container>
			  <Row className="justify-content-md-center">
			    <Col xs lg="8">
			     	<p className="textFooter">© {(new Date().getFullYear())}</p>
			    </Col>			    
			  </Row>
			</Container>
		);
	}
}

export default FooterComponent;