import React, {Component} from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import './../App.css';

class HeaderComponent extends Component {
	render() {
		return (
			<Container id="AppHeader">
			  <Row className="justify-content-md-center">
			    <Col xs lg="8" className="HeaderContent">
			     	<h3>GLobal RH Teste</h3>
			    </Col>
			  </Row>
			</Container>
		);
	}
}

export default HeaderComponent;