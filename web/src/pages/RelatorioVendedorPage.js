import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { Container, Row, Col, Table } from 'react-bootstrap';
import ConfigAmbiente from './../ConfigAmbiente';
import './../App.css';

class RelatorioVendedorPage extends Component {

	constructor(props) {
      super(props);

      this.state = {
      	error: null,
      	isLoaded: false,
      	vendedor: {}
      }

  	}

	getVendedor (idVendedor) {
		fetch(ConfigAmbiente.API_URL + '/vendedores/relatorio/vendedor/'+idVendedor)
		  .then(function(response) {
		    return response.json();
		  })
		  .then((json) => {
		  	if (json.data) {
		  		this.setState({
					vendedor: json.data,
					isLoaded: true
				});
		  	} else {
		  		this.setState({
					vendedor: {vendedor: '', rede: null},
					isLoaded: true
				});
		  	}		    
		  });
	}

	componentDidMount(){
		const { match: { params } } = this.props;
		this.getVendedor(params.id);		
	}

	render() {

		const { error, isLoaded, vendedor } = this.state;
		let linhasTr = [];
		let limiteLinhas = 1;

		if (vendedor.rede && vendedor.rede.esquerda > vendedor.rede.direita) {
			limiteLinhas = vendedor.rede.esquerda.length;
		}

		if (vendedor.rede && vendedor.rede.esquerda < vendedor.rede.direita) {
			limiteLinhas = vendedor.rede.direita.length;
		}

		

	   	let i;
		for (i = 0; i < limiteLinhas; i++) {
		  linhasTr.push(i);
		}


		if (error) {
	      return <div>Error: {error.message}</div>;
	    } else if (!isLoaded) {
	      return <div className="textCenter">Carregando...</div>;
	    } else {
			return (
				<Container>
				  <Row className="justify-content-md-center">
				    <Col xs lg="8">
				    	<div className="AppContent">
							<h2>Relatório rede binária do vendedor</h2>
							<Table striped bordered hover>
								<thead>
									<tr>
										<th colSpan="2">Pts perna esquerda do <b>{vendedor.vendedor}</b></th>
										<th colSpan="2">Pts perna direita do <b>{vendedor.vendedor}</b></th>
									</tr>
								</thead>
								<tbody>
									{linhasTr.map((val, i) =>

										<tr key={val}>
											<td>{vendedor.rede.esquerda[i]}</td>
											<td>{(vendedor.rede.esquerda[i]) ? vendedor.pontos_por_indicado + ' pts' : '' }</td>
											<td>{vendedor.rede.direita[i]}</td>
											<td>{(vendedor.rede.direita[i]) ? vendedor.pontos_por_indicado + ' pts' : '' }</td>											
										</tr>
									)}
									<tr>
										<td><b>TOTAL:</b></td>
										<td>{vendedor.rede.esquerda.length * vendedor.pontos_por_indicado} pts</td>
										<td><b>TOTAL:</b></td>
										<td>{vendedor.rede.direita.length * vendedor.pontos_por_indicado} pts</td>
									</tr>
								</tbody>
							</Table>
							<Link to="/" className="btn">Voltar</Link>
						</div>
				    </Col>			    
				  </Row>
				</Container>
			);
		}
	}
}

export default RelatorioVendedorPage;