import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { Container, Row, Col } from 'react-bootstrap';
import RelatorioComponent from './../components/RelatorioComponent';

class HomePage extends Component {
	render() {
		return (
			<Container>
			  <Row className="justify-content-md-center">
			    <Col xs lg="8">
			      <Link to="/vendedor" className="btn btn-success">Novo Vendedor</Link>
			      <RelatorioComponent/>
			    </Col>
			  </Row>
			</Container>			
		);
	}
}

export default HomePage;