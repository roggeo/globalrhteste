import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import ConfigAmbiente from './../ConfigAmbiente';
import swal from 'sweetalert';
import './../App.css';

class VendedorPage extends Component {

	constructor(props) {
      super(props);

      this.state = {
      	error: null,
      	isLoaded: false,
      	listaVendedores: []
      }

  	}

	getListaVendedores () {
		fetch(ConfigAmbiente.API_URL + '/vendedores/permitidos')
		  .then(function(response) {
		    return response.json();
		  })
		  .then((json) => {
		    this.setState({
				listaVendedores: json.data,
				isLoaded: true
			});
		  });
	}

	salvar () {
		let formData = new FormData(this.formVendedor);
		let dataObject = {};

		for (const [key, value]  of formData.entries()) {
		    dataObject[key] = value;
		}

		if (dataObject.nome.trim() === '') {
			swal("Ops!", "Favor preencha o formulário corretamente.", "warning");
			return;
		}

		fetch(ConfigAmbiente.API_URL + '/vendedores', {
	        method: 'POST',
	        mode: 'cors',
	        cache: 'no-cache',
	        credentials: 'same-origin',
	        headers: {
	            'Content-Type': 'application/json',
	        },
	        redirect: 'follow',
	        referrer: 'no-referrer',
	        body: JSON.stringify(dataObject)
	    })
	    .then(response => response.json())
	    .then(() => {
	    	swal("Cadastrado!", "Novo vendedor foi cadastrado.", "success");
	    }).catch(() => {
	    	swal("Ops!", "Algo deu errado.", "error");
	    });
	}

	componentDidMount(){
		this.getListaVendedores();		
	}

	render() {

		const { error, isLoaded, listaVendedores } = this.state;

		if (error) {
	      return <div>Error: {error.message}</div>;
	    } else if (!isLoaded) {
	      return <div className="textCenter">Carregando...</div>;
	    } else {
			return (
				<Container>
				  <Row className="justify-content-md-center">
				    <Col xs lg="8">
				    	<div className="AppContent">
							<h2>Cadastrar novo vendedor</h2>
					     	<Form ref={(f) => this.formVendedor = f}>	  
							  <Form.Group>
							    <Form.Label>Nome Vendedor <span>(*)</span></Form.Label>
							    <Form.Control name="nome"/>
							  </Form.Group>

							  <Form.Group>
							    <Form.Label>Indicador</Form.Label>
							      <Form.Control name="indicador" as="select">
							        <option>Selecione...</option>
							        {listaVendedores.map((val) =>
							        	<option key={val.id} value={val.id}>{val.nome}</option>
							        )}
							    </Form.Control>
							  </Form.Group>
							  <Button variant="success" type="button" onClick={() => this.salvar()}>
							    Salvar
							  </Button>
							  <Link to="/" className="btn">Voltar</Link>
							</Form>
						</div>
				    </Col>			    
				  </Row>
				</Container>
			);
		}
	}
}

export default VendedorPage;